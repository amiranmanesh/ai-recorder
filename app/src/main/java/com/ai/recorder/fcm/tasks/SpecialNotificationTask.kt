package com.ai.recorder.fcm.tasks

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.ai.recorder.R
import com.ai.recorder.utils.toMap
import io.reactivex.Single
import ir.malv.utils.Pulp
import java.net.URL
import java.util.*


class SpecialNotificationTask(
    private val context: Context,
    private val workerParameters: WorkerParameters
) : RxWorker(context, workerParameters) {

    override fun createWork(): Single<Result> {
        try {
            val data = workerParameters.inputData.keyValueMap // All fcm messages here
            if (data.isEmpty()) return Single.just(Result.failure())
            val specialData = data["keys"].toString().toMap()

            val title: String? = specialData["title"]
            val body: String? = specialData["body"]
            val url: String? = specialData["url"]
            val largeIconUrl: String? = specialData["large_icon_url"]
            return if (largeIconUrl.isNullOrEmpty()) {
                sendNotification(title, body, url)
                Single.just(Result.success())
            } else {
                Single.fromCallable {
                    val inStream = URL(largeIconUrl).openStream()
                    BitmapFactory.decodeStream(inStream)
                }.map {
                    sendNotification(title, body, url, it)
                    Result.success()
                }
            }

        } catch (e: Exception) {
            Pulp.error(TAG, "Error", e)
            return Single.just(Result.failure())
        }
    }


    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(
        messageTitle: String?,
        messageBody: String?,
        url: String?,
        largeIcon: Bitmap?
    ) {
        val resultIntent = Intent(Intent.ACTION_VIEW)
        resultIntent.data = Uri.parse(url)
        val pending =
            PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val channelId = context.getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setLargeIcon(largeIcon)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pending)


        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel)
        }

        Objects.requireNonNull(notificationManager)
            .notify(0 /* ID of notification */, notificationBuilder.build())
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageTitle: String?, messageBody: String?, url: String?) {
        val resultIntent = Intent(Intent.ACTION_VIEW)
        resultIntent.data = Uri.parse(url)
        val pending =
            PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val channelId = context.getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pending)


        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel)
        }

        Objects.requireNonNull(notificationManager)
            .notify(0 /* ID of notification */, notificationBuilder.build())
    }

    companion object {
        const val TAG = "SpecialAdTask"
    }
}
