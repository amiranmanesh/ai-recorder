package com.ai.recorder.fcm

import android.content.Context
import androidx.work.*
import com.ai.recorder.fcm.tasks.AntiMistakeTask
import com.ai.recorder.fcm.tasks.SpecialNotificationTask
import com.google.firebase.messaging.RemoteMessage
import ir.malv.utils.Pulp
import kotlin.reflect.KClass

object FcmMessageParser {

    /**
     * Function will get the message from fcm messaging class and returns a reference
     *  to the proper worker.
     *  NOTE: It does not return an object, it will return an reference to the class.
     */
    fun getWorker(message: RemoteMessage?): KClass<out RxWorker>? {
        if (message?.data.isNullOrEmpty()) {
            Pulp.debug(TAG, "Remote message looks empty. Ignoring parsing.")
            return null
        } else if (message?.data?.containsKey(TYPE_KEY) != true) {
            Pulp.error(TAG, "Message doesn't have 'type' key. Ignoring parsing.")
        }
        return when (message?.data?.get(TYPE_KEY)) {
            TYPE_KEY_ANTI_MISTAKE -> AntiMistakeTask::class
            TYPE_KEY_SPECIAL_NOTIFICATION -> SpecialNotificationTask::class
            else -> null
        }
    }


    /**
     * Function uses the reference extension to start a work.
     * NOTE: Reference is a subtype of WorkManager [Worker]
     */
    fun KClass<out RxWorker>?.execute(data: Map<String, String>, context: Context) {
        if (this == null) return
        val work = OneTimeWorkRequest.Builder(this.java)
            .setInputData(Data.Builder().putAll(data).build())
            .build()
        WorkManager.getInstance(context).beginWith(work).enqueue()
    }

    fun KClass<out RxWorker>?.execute(context: Context) {
        if (this == null) return
        val work = OneTimeWorkRequest.Builder(this.java)
            .build()
        WorkManager.getInstance(context).beginWith(work).enqueue()
    }

    private const val TYPE_KEY = "type"
    private const val TYPE_KEY_ANTI_MISTAKE = "flavor"
    private const val TYPE_KEY_SPECIAL_NOTIFICATION = "special_notification"

    private const val TAG = "MessageParser"
}