package com.ai.recorder.fcm.tasks

import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.ai.recorder.utils.StorageHelper
import io.reactivex.Single
import ir.malv.utils.Pulp


/**
 * Having message type as `flavor` will cause this task to be executed.
 * so the message will be:
 * data={
 *   type=flavor,
 *   app_flavor=true/false
 *   app_flavor_message="messages"
 * }
 */
class AntiMistakeTask(
    private val context: Context,
    private val workerParameters: WorkerParameters
) : RxWorker(context, workerParameters) {

    override fun createWork(): Single<Result> {
        workerParameters.inputData.keyValueMap.let {
            try {
                (it["app_flavor"].toString().toBoolean()).let { continuationAllowed ->
                    Pulp.info(TAG, "ContinuationAllowed: ${!continuationAllowed}")
                    StorageHelper(context).setBool("app_flavor", continuationAllowed)
                    Pulp.info(TAG, "Flavor configured")
                }
                (it["app_flavor_message"].toString()).let { continuationAllowedMessage ->
                    Pulp.info(TAG, "FlavorMessage: $continuationAllowedMessage")
                    StorageHelper(context).setString("app_flavor_message", continuationAllowedMessage)
                    Pulp.info(TAG, "FlavorMessage configured")
                }
            } catch (e: Exception) {
                Pulp.error(TAG, "Could not handle data") {
                    "Cause" to e.message
                }

            }
        }

        return Single.just(Result.success())
    }

    companion object {
        const val TAG = "FlavorTask"
    }
}
