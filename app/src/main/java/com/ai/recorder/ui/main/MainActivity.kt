package com.ai.recorder.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder
import cafe.adriel.androidaudiorecorder.model.AudioChannel
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate
import cafe.adriel.androidaudiorecorder.model.AudioSource
import com.ai.recorder.R
import com.ai.recorder.ui.base.BaseActivity
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import ir.malv.utils.Pulp
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : BaseActivity() {

    companion object {
        var TAG = "MainActivity"
    }

    private var checkFlag: Boolean = false
    private var numberIndex: Int = 0
    private val NUMBER_MIN = 100
    private val NUMBER_MAX = 199
    private val NUMBER_COUNT = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkFlag = false
        checkNumbersLeft()

        mainRecordButton.setOnClickListener {
            if (numberIndex <= NUMBER_MAX) onRecordButtonClick()
            else checkNumbersLeft()
        }
        mainNumberPlus.setOnClickListener {
            if (numberIndex < NUMBER_MAX) onPlusClick()
            else checkNumbersLeft()
        }
        mainNumberMinus.setOnClickListener {
            onMinusClick()
        }
        mainNumberDelete.setOnClickListener {
            if (numberIndex <= NUMBER_MAX) onDeleteClick()
            else checkNumbersLeft()
        }

        mainSwipeRefresh.setOnRefreshListener {
            checkNumbersLeft()
        }

        mainNumberFolder.setOnClickListener {
            openFolder()
        }

    }

    override fun onResume() {
        super.onResume()
        if (checkFlag) {
            Pulp.info(TAG, "onResume -> checkFlag: $checkFlag")
            checkLastSavedItem()
        } else if (numberIndex <= NUMBER_MAX)
            updateNumberText()
    }

    private fun onDeleteClick() {
        analytics("onDeleteClick")
        showSimpleDialog(
            "آیا میخواهید تمام فایل های ذخیره شده مربوط به این عدد را پاک کنید؟",
            DialogInterface.OnClickListener { dialog, _ ->
                val files = getFileDir()
                if (files.exists()) {
                    try {
                        files.deleteRecursively()
                        toast("با موفقیت پاک شد.")
                        updateNumberText()
                        dialog.dismiss()
                    } catch (e: Exception) {
                        toast("مشکلی رخ داده است.")
                    }
                }
            }
        )
    }

    private fun checkSubListItem(): Int {
        val files = getFileDir()
        return if (files.exists())
            files.listFiles().size
        else 0
    }

    private fun checkNumbersLeft() {
        analytics("checkNumbersLeft")
        mainSwipeRefresh.isRefreshing = false
        for (i in NUMBER_MIN..NUMBER_MAX) {
            numberIndex = i
            val value = checkSubListItem()
            if (value < NUMBER_COUNT) {
                numberIndex = i
                updateNumberText()
                return
            }
        }

        //ended
        numberIndex = NUMBER_MAX + 1
        mainNumberLeft.text = "با تشکر از همکاری شما در این پروژه"
        mainNumberText.text = ""
    }

    @SuppressLint("SetTextI18n")
    private fun updateNumberText() {
        val value = checkSubListItem()
        mainNumberText.text = "$numberIndex"
        mainNumberLeft.text = " تعداد فایل های ذخیره شده : $value"
        Firebase.analytics.logEvent("DATA") {
            param("number", numberIndex.toString())
            param("item", value.toString())
        }
    }

    private fun onMinusClick() {
        analytics("onMinusClick")
        if (numberIndex > NUMBER_MIN) {
            numberIndex--
            updateNumberText()
        }
    }

    private fun onPlusClick() {
        analytics("onPlusClick")
        if (numberIndex < NUMBER_MAX) {
            numberIndex++
            updateNumberText()
        }
    }

    private fun onRecordButtonClick() {
        analytics("onRecordButtonClick")
        val yourAppDir = getFileDir()
        if (!yourAppDir.exists() && !yourAppDir.isDirectory) { // create empty directory
            if (yourAppDir.mkdirs()) {
                Pulp.info(TAG, "App dir created")
            } else {
                Pulp.info(TAG, "Unable to create app dir!")
                toast("مشکلی در دسترسی به حافظه رخ داده است...")
                return
            }
        } else {
            Pulp.info(TAG, "App dir already exists")
        }

        var value = checkSubListItem()
        value += 1
        val filePath: String = yourAppDir.toString() + File.separator.toString() + "${value}.wav"
        val color = resources.getColor(R.color.colorPrimaryDark)
        val requestCode = 0

        checkFlag = true
        AndroidAudioRecorder.with(this) // Required
            .setFilePath(filePath)
            .setColor(color)
            .setRequestCode(requestCode) // Optional
            .setSource(AudioSource.MIC)
            .setChannel(AudioChannel.STEREO)
            .setSampleRate(AudioSampleRate.HZ_48000)
            .setAutoStart(true)
            .setKeepDisplayOn(true) // Start recording
            .record()

    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        Pulp.info(TAG, "onActivityResult -> requestCode: $requestCode /value: $resultCode")
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) { // Great! User has recorded and saved the audio file
                if (checkFlag) {
                    Pulp.info(TAG, "onActivityResult -> checkFlag: $checkFlag")
                    checkLastSavedItem()
                }
            } else if (resultCode == Activity.RESULT_CANCELED) { // Oops! User has canceled the recording
            }
        }
    }

    private fun checkLastSavedItem() {
        analytics("checkLastSavedItem")
        checkFlag = false
        try {
            val value = checkSubListItem()
            Pulp.info(TAG, "checkLastSavedItem -> numberIndex: $numberIndex /value: $value")
            val file = getFileDir()
            if (file.exists()) {
                Pulp.info(TAG, "checkLastSavedItem -> file exist")
                var fileSize = java.lang.String.valueOf(file.length() / 1024).toInt()
                fileSize /= 1024
                if (fileSize < 20) {
                    Pulp.info(
                        TAG,
                        "checkLastSavedItem -> file size($fileSize) < 20MB / value: $value"
                    )
                    if (value == NUMBER_COUNT) {
                        numberIndex++
                        Pulp.info(
                            TAG,
                            "checkLastSavedItem -> new value: $value / numberIndex: $numberIndex "
                        )
                    }
                    if (numberIndex <= NUMBER_MAX) updateNumberText()
                    else checkNumbersLeft()
                    toast("با موفقیت ذخیره شد.")
                } else {
                    //fucked up
                    if (file.delete()) {
                        toast("مشکلی در ذخیره سازی داده است. لطفا دوباره تلاش کنید...")
                    } else {
                        Pulp.info(TAG, "file not Deleted ")
                    }
                }
            } else {
                Pulp.info(TAG, "file not exists ")
            }

        } catch (e: Exception) {
            toast("مشکلی رخ داده است. لطفا دوباره تلاش کنید...")
        }

    }

    private fun getFileDir(): File {
        return File(getRootPath() + File.separator.toString() + "$numberIndex")
    }


    private fun getRootPath(): String {
        Pulp.info(TAG, "sdk int: ${Build.VERSION.SDK_INT}")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            Environment.isExternalStorageManager()
        }
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            .toString() + File.separator.toString() + "AI-Recorder"
    }


    private fun openFolder() {
        analytics("onOpeningFolder")
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            val yourAppDir = File(getRootPath())
            if (!yourAppDir.exists() && !yourAppDir.isDirectory) { // create empty directory
                if (yourAppDir.mkdirs()) {
                    Pulp.info(TAG, "App dir created") {
                        "path" to yourAppDir.toString()
                    }
                } else {
                    Pulp.info(TAG, "Unable to create app dir!")
                    toast("مشکلی در دسترسی به حافظه رخ داده است...")
                    return
                }
            } else {
                Pulp.info(TAG, "App dir already exists") {
                    "path" to yourAppDir.toString()
                }
            }
            val mydir: Uri = Uri.parse(yourAppDir.toString())
            intent.setDataAndType(mydir, "*/*") // or use */*
            this.startActivity(Intent.createChooser(intent, "Open folder"));
        } catch (e: Exception) {
            Pulp.error(TAG, "Error", e)
            toast("مشکلی رخ داده است. به فولدر Downloads مراجعه کنید")
        }
    }

    private fun analytics(action: String) {
        Firebase.analytics.logEvent(TAG) {
            param("action", action)
        }
    }
}
