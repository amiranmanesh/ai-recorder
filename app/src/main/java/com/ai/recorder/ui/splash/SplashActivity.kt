package com.ai.recorder.ui.splash

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.ai.recorder.BuildConfig
import com.ai.recorder.R
import com.ai.recorder.ui.base.BaseActivity
import com.ai.recorder.ui.main.MainActivity
import com.ai.recorder.utils.StorageHelper
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity() {
    private val REQUEST_CODE = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splashVersionCode.text = BuildConfig.VERSION_NAME

        val flavor = StorageHelper(this).getBool("app_flavor", false)
        val flavorMessage = StorageHelper(this).getString("app_flavor_message", null)
        if (flavor) {
            Firebase.analytics.setUserProperty("app_flavor", "true")
            showMessageOKCancel(flavorMessage ?: "دسترسی به اپلیکیشن موقتا قطع شده است", { _, _ ->
                this@SplashActivity.finish()
            }, { _, _ ->
                this@SplashActivity.finish()
            })
            return
        } else {
            Firebase.analytics.setUserProperty("app_flavor", "false")
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermissions()) {
                nextActivity()
            }
        } else {
            nextActivity()
        }
    }

    private fun nextActivity() {
        Handler().postDelayed({
            //go to new activity
            startActivity(Intent(this, MainActivity::class.java))
            finish()

        }, 500)
    }


    private fun checkPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (checkPermission()) {
                true
            } else {
                requestPermission()
                false
            }
        }
        return false
    }


    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE -> if (grantResults.isNotEmpty()) {

                val audioAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val writeAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                val readAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED
                if (audioAccepted && writeAccepted && readAccepted) {
                    nextActivity()

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO) || shouldShowRequestPermissionRationale(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ) || shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
                        ) {
                            showMessageOKCancel("برای استفاده از این اپلیکیشن باید این دسترسی ها را قبول کنید.",
                                { _, _ ->
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(
                                            arrayOf(
                                                Manifest.permission.RECORD_AUDIO,
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                Manifest.permission.READ_EXTERNAL_STORAGE
                                            ),
                                            REQUEST_CODE
                                        )
                                    }
                                },
                                { _, _ ->
                                    finish()
                                })
                            return
                        }
                    }
                }
            }
        }
    }

    private fun showMessageOKCancel(
        message: String,
        okListener: DialogInterface.OnClickListener,
        noListener: DialogInterface.OnClickListener
    ) {
        androidx.appcompat.app.AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("باشه", okListener)
            .setNegativeButton("بعدا", noListener)
            .setCancelable(false)
            .create()
            .show()
    }

    companion object {
        var TAG = "SplashActivity"
    }
}
