package com.ai.recorder.ui.base

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.ai.recorder.R
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import ir.malv.utils.Pulp
import kotlin.system.exitProcess


@Suppress("DEPRECATION")
abstract class BaseActivity : AppCompatActivity() {

    companion object {
        var TAG = "BaseActivity"
    }

    /**
     *
     * To use and configure font
     * Calligraphy used to configure font.
     */
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    /**
     * If want to add some features to current toolbar
     * we will get it using this function
     *
     * @return the default toolbar
     */
//    protected val defaultToolbar: Toolbar
//        get() = findViewById<View>(R.id.toolbar) as Toolbar

    /**
     * Logging into fireBase analytics for each activity
     */
    //Begin
    override fun onRestart() {
        super.onRestart()
        log(this.localClassName, "onRestart")
    }

    override fun onResume() {
        super.onResume()
        log(this.localClassName, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(this.localClassName, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(this.localClassName, "onDestroy")
    }
    //End

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun setStatusBarGradiant(activity: Activity) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                val background = activity.resources.getDrawable(R.drawable.gradient_full_back)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = activity.resources.getColor(android.R.color.transparent)
                window.navigationBarColor = activity.resources.getColor(android.R.color.black)
                window.setBackgroundDrawable(background)
            }
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "setStatusBarGradiant", e)
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set orientation to Portrait
        log(this.localClassName, "onCreate")
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
//            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        }

        // Handle uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler { paramThread, paramThrowable ->
            // If any exception was unhandled this will be logged
            // Since all activities extend this class, it is possible to see all errors.
            Pulp.wtf(
                TAG,
                "Unhandled exception occurred.\n" +
                        "Thread: " + paramThread.name,
                Exception(paramThrowable)
            )
            paramThrowable.printStackTrace()
            // Without this command this code does not work.
            exitProcess(2)
        }

//        val fabric = Fabric.Builder(this)
//            .kits(Crashlytics())
//            .debuggable(true)
//            .build()
//        Fabric.with(fabric)

        supportActionBar?.hide()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            setStatusBarGradiant(this)
        }

    }

    fun isShowFragment(tag: String): Boolean {
        val fragment = fragmentManager.findFragmentByTag(tag)
        return fragment != null
    }

    fun hideFragment(tag: String) {
        val fragment = fragmentManager.findFragmentByTag(tag)
        val ft = fragmentManager.beginTransaction()
        fragment?.let { ft.hide(it) }
        ft.commit()
    }

//    fun hideFragmentWithAnim(tag: String) {
//        val fragment = fragmentManager.findFragmentByTag(tag)
//        val ft = fragmentManager.beginTransaction()
//        ft.setCustomAnimations(R.anim.enter, R.anim.exit)
//        fragment?.let { ft.hide(it) }
//        ft.commit()
//    }
//
//    fun removeFragment(tag: String) {
//        val fragment = fragmentManager.findFragmentByTag(tag)
//        val ft = fragmentManager.beginTransaction()
//        ft.setCustomAnimations(R.anim.enter, R.anim.exit)
//        fragment?.let { ft.remove(it) }
//        ft.commit()
//    }

    fun removeFragmentNoAnim(tag: String) {
        val fragment = fragmentManager.findFragmentByTag(tag)
        val ft = fragmentManager.beginTransaction()
        fragment?.let { ft.remove(it) }
        ft.commit()
    }

    fun showFragment(tag: String) {
        val fragment = fragmentManager.findFragmentByTag(tag)
        val ft = fragmentManager.beginTransaction()
        fragment?.let { ft.show(it) }
        ft.commit()
    }


    /**
     * Get Called when we want to hide softKeyboard
     */
    fun hideKeyboard(activity: Activity) {
        try {
            val view = activity.currentFocus
            if (view != null) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        } catch (e: Exception) {
            Pulp.debug(TAG, "hideKeyboard error", e)
        }
    }


    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun longToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    /**
     * Show a simple dialog
     */
    protected fun showSimpleDialog(
        message: String,
        yesAction: DialogInterface.OnClickListener
    ) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("بله", yesAction)
            .setNegativeButton("خیر") { dialog, _ -> dialog.dismiss() }
            .setCancelable(true)
            .create()
            .show()
    }

    /**
     * To log an event to analytics.
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(TAG, "$screenName -> $event")
    }
}