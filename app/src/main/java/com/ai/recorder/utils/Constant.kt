package com.ai.recorder.utils


object Constant {

    // Debugging log tag
    var DEBUG_MODE = true
    const val LOG_TAG = "AI-Recorder"
}