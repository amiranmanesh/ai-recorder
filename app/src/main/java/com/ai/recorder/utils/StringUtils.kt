package com.ai.recorder.utils

import android.util.Base64
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.malv.utils.Pulp
import org.json.JSONException
import org.json.JSONObject
import java.net.MalformedURLException
import java.net.URL
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols


fun String.toMap(
): Map<String, String> {
    if (isNullOrEmpty() || this == "null") return emptyMap()
    return try {
        Gson().fromJson<HashMap<String, String>>(
                this,
                object : TypeToken<HashMap<String, String>>() {}.type
        )
    } catch (e: JSONException) {
        Pulp.error("String.toMap", "Failed to convert message to map", e)
        emptyMap()
    }
}

fun String.toURL(
) = try {
    URL(this)
} catch (e: MalformedURLException) {
    e.printStackTrace()
    null
}

fun String.toJSONObject(
) = try {
    JSONObject(this)
} catch (e: MalformedURLException) {
    e.printStackTrace()
    JSONObject()
}

fun String.thousandSeparator(
): String {
    if (isNullOrEmpty() || this == "null") return ""
    return try {
        val number = java.lang.Long.parseLong(this)
        val decimalFormat = DecimalFormat()
        val decimalFormatSymbol = DecimalFormatSymbols()
        decimalFormatSymbol.groupingSeparator = ','
        decimalFormat.decimalFormatSymbols = decimalFormatSymbol
        decimalFormat.format(number)
    } catch (ex: Exception) {
        this
    }
}

fun String.base64Encode(): String {
    return Base64.encodeToString(this.toByteArray(charset("UTF-8")), Base64.DEFAULT)
}

fun String.base64Decode(): String {
    return Base64.decode(this, Base64.DEFAULT).toString(charset("UTF-8"))
}
