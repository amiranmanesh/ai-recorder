package com.ai.recorder.utils

import android.util.Log
import com.ai.recorder.BuildConfig
import ir.malv.utils.Pulp

class DebugLogcatHandler : Pulp.LogHandler {
    override fun onLog(
        level: Pulp.Level,
        tags: List<String>,
        message: String,
        t: Throwable?,
        data: Pulp.LogData,
        time: Long
    ) {
        if (!BuildConfig.DEBUG) {
            // Avoid logging to Logcat if build type is not debug
            return
        }
        val logMessage = logMessage(message, data)
        when (level) {
            Pulp.Level.I -> if (t != null) Log.i(tag(tags), logMessage, t) else Log.i(
                tag(tags),
                logMessage
            )
            Pulp.Level.D -> if (t != null) Log.d(tag(tags), logMessage, t) else Log.d(
                tag(tags),
                logMessage
            )
            Pulp.Level.W -> if (t != null) Log.w(tag(tags), logMessage, t) else Log.w(
                tag(tags),
                logMessage
            )
            Pulp.Level.E -> if (t != null) Log.e(tag(tags), logMessage, t) else Log.e(
                tag(tags),
                logMessage
            )
            Pulp.Level.WTF -> if (t != null) Log.wtf(tag(tags), logMessage, t) else Log.wtf(
                tag(tags),
                logMessage
            )
        }
    }

    private fun tag(tags: List<String>) = "Plogger(${tags.joinToString(",")})"

    private fun logMessage(
        message: String,
        logData: Pulp.LogData
    ): String {
        val str = StringBuilder(message)
        if (logData.data.isNotEmpty()) {
            str.append(" {" + logData.data.map { "${it.key}=${it.value}" }.joinToString(", ") + "}")
        }
        return str.toString()
    }

}