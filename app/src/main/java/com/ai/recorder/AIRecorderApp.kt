package com.ai.recorder

import androidx.multidex.MultiDexApplication
import com.ai.recorder.utils.DebugLogcatHandler
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import ir.malv.utils.Pulp

class AIRecorderApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        Pulp.init(this).addHandler(DebugLogcatHandler())

        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/iransans.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

        // Initialize Firebase app
        FirebaseApp.initializeApp(this)

        FirebaseMessaging.getInstance().subscribeToTopic("app_user").addOnSuccessListener {
            Pulp.debug(TAG, "Subscribed to topic app_user")
        }

        FirebaseMessaging.getInstance().subscribeToTopic(BuildConfig.VERSION_CODE.toString())
            .addOnSuccessListener {
                Pulp.debug(TAG, "Subscribed to topic ${BuildConfig.VERSION_CODE}")
            }

    }

    companion object {
        const val TAG = "Application"
    }
}